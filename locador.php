<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>LOCADOR</title>
    <!-- Inserindo o BootStrap no Código - Chamando as páginas-->
 <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

 
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link rel=stylesheet href="css/creative.css">
 


    <!-- Final do BootStrap-->
</head>
<body>
    
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Contrato Virtual</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="locador.php">Locador</a>
                    </li>
                      <li class="nav-item">
                      <a class="nav-link" href="locatario.php">Locatário</a>
                    </li>
                  </ul>
                </div>
                 <div margin="left">
                    <ul>
                         <a class="nav-link" href="index.php">SAIR</a>
                    </ul>
                </div>
              </nav>
       
               <!----> 
            
                <!-- Address form -->
                <div class="container">  
                 <form class="form-horizontal" method="POST" action="actions/CadLocador.php">
                     <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6"><br><br>
                <div align="center" class="container">
                    <h3> Seja Bem Vindo Locador</h3>
                </div>
                <h2 align="center"> CONTRATO DE LOCAÇÃO DE IMÓVEL RESIDENCIAL</h2> <bR><br>
                
            </div> 
        </div>
                       
                            <!-- Locador-->
                <div class="row">
                    <div class="col-md-3 field-label-responsive">
                        <b><label for="name">Locador:</label></b>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                 <div class="input-group-addon" style="width: 2.6rem"></div><br>
                                <input type="text" name="nome" class="form-control" id="nome"  required autofocus>
                            </div>
                        </div>
                    </div> 
                </div>

                            

                            <!--ESTADO CIVIL-->
                <div class="row">
                     <div class="col-md-3 field-label-responsive">
                        <b><label for="name">Estado Civil:</label></b>
                    </div>
                        <div class="col-md-6">
                         <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                    <select id="estCivil" name="estCivil" class="input-xlarge">
                                        <option value="" selected="selected">Selecione seu Estado Civil</option>
                                        <option value="solteiro">Solteiro</option>
                                        <option value="casado">Casado</option>
                                        <option value="divociado">Divociado</option>
                                    </select><br>
                                       
                            </div>
                         </div>
                        </div>
                </div>
                            
            
                            <!-- address-line2 input-->
                            <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">Profissão:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="text" name="profissao" class="form-control" id="profissao" placeholder="" required autofocus>
                             </div>
                         </div>
                     </div>
                  </div>
                            <!-- RG-->
                            <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">RG:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="number" name="rg" class="form-control" id="rg" placeholder="0000000-0" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>
                            <!-- postal-code input-->
                           <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">CPF:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="int" name="codPostal" class="form-control" id="codPostal" placeholder="000.000.000-00" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>
                            <!-- country select -->
                            <div class="row">
                     <div class="col-md-3 field-label-responsive">
                               <b> <label for="name">Estado:</label></b>
                               </div>
                                       <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                    <select id="estado" name="estado" class="input-xlarge">
                                        <option value="" selected="selected">Selecione seu Estado</option>
                                        <option value="Acre">Acre</option>
                                        <option value="Alagoas">Alagoas</option>
                                        <option value="Amapá">Amapá</option>
                                        <option value="Amazonas">Amazonas</option>
                                        <option value="Bahia">Bahia </option>
                                        <option value="Ceará">Ceará</option>
                                        <option value="Distrito Federal">Distrito Federal</option>
                                        <option value="Espírito Santo">Espírito Santo</option>
                                        <option value="Goiás">Goiás</option>
                                        <option value="Maranhão">Maranhão</option>
                                        <option value="Mato Grosso">Mato Grosso</option>
                                        <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                                        <option value="Minas Gerais">Minas Gerais</option>
                                        <option value="Pará">Pará</option>
                                        <option value="Paraíba">Paraíba </option>
                                        <option value="Paraná">Paraná</option>
                                        <option value="Pernambuco">Pernambuco </option>
                                        <option value="Piauí">Piauí</option>
                                        <option value="Rio de Janeiro">Rio de Janeiro</option>
                                        <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                                        <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                                        <option value="Rondônia">Rondônia</option>
                                        <option value="Roraima">Roraima</option>
                                        <option value="Santa Catarina">Santa Catarina</option>
                                        <option value="São Paulo">São Paulo</option>
                                        <option value="Sergipe">Sergipe</option>
                                        <option value="Tocantins">Tocantins </option>
                                    </select><br>     
                            </div>
                        </div>
                     </div>
                            </div>

                             <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">Cidade:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="text" name="cidade" class="form-control" id="cidade" placeholder="" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>

                   <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">Bairro:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="text" name="bairro" class="form-control" id="bairro" placeholder="" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>

                   <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">Endereço:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="text" name="endereco" class="form-control" id="endereco" placeholder="" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>
                   <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">Número:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="text" name="numero" class="form-control" id="numero" placeholder="" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>
                   <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">Inicio Da Locação:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="date" name="iniciolocacao" class="form-control" id="iniciolocacao" placeholder="" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">Termino Da Locação:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="date" name="fimlocacao" class="form-control" id="fimlocacao" placeholder="" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>
                    <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">Valor:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="text" name="valor" class="form-control" id="valor" placeholder="" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>
             

                             <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              <br><button  type="submit" class="btn btn-success" style="margin-left:40px" href="index.php"><i class="fa "></i>Enviar</button><br><br>
            </div>
        
        </div>
    </form>             
            </fieldset>
                     
            </div>
            </div>

            </body>
            
            </html>