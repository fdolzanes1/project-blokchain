<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link rel=stylesheet href="../css/creative.css">
 
    <link rel="icon" href="img/img_cad.jpg" style="height:50px">
    <title>Cadastro Usuario</title>
</head>
<body>
        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../index.php">Inicio</a>
        </button>
      </div>
    </nav>
<div class="container">
    <form class="form-horizontal" method="POST" action="../actions/usuarioCadastrar.php">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6"><br><br>
                <h2 align="center">Cadastre-se</h2>
                <hr>
            </div> 
        </div>
        <div class="row">
            <div class="col-md-3 field-label-responsive">
                <b><label for="name">CPF.</label></b>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-credit-card"></i></div>
                        <input type="text" name="cpf" class="form-control" onkeypress="mascara(this,'###.###.###-##');return SomenteNumero(event)" maxlength="14" placeholder="xxx.xxx.xxx-xx" required autofocus>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-control-feedback">
                    <span class="text-danger align-middle"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 field-label-responsive">
                <b><label for="name">Nome.</label></b>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                        <input type="text" name="nome" class="form-control" id="nome"
                               placeholder="nome completo" required autofocus>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-control-feedback">
                    <span class="text-danger align-middle"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 field-label-responsive">
                <b><label for="email">E-Mail.</label></b>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
                        <input type="text" name="email" class="form-control" placeholder="user@example.com" required autofocus>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-control-feedback">
                    <span class="text-danger align-middle"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 field-label-responsive">
                <b><label for="password">Telefone.</label></b>
            </div>
            <div class="col-md-6">
                <div class="form-group has-danger">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-phone"></i></div>
                        <input type="text" name="telefone" class="form-control" onkeypress="mascara(this,'## #####-####');
                            return SomenteNumero(event)" placeholder="(xx) xxxxx-xxxx" maxlength="13" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 field-label-responsive">
                <b><label for="password">Senha</label></b>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
                        <input type="password" name="senha" class="form-control" placeholder="password" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-success" style="margin-left:40px"><i class="fa fa-user-plus"></i>Cadastrar</button>
            </div>
        
        </div>
    </form>
</div>
</body>
