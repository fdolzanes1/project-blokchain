<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>LOCATÁRIO</title>
    <!-- Inserindo o BootStrap no Código - Chamando as páginas-->
 <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

 
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link rel=stylesheet href="css/creative.css">
    <!-- Final do BootStrap-->
</head>
<body>
    
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Contrato Virtual</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="locador.php">Locador</a>
                    </li>
                      <li class="nav-item">
                      <a class="nav-link" href="locatario.php">Locatário</a>
                    </li>
                      <li class="nav-item">
                      <a class="nav-link" href="#">Fiador</a>
                    </li>
                      <li class="nav-item">
                      <a class="nav-link" href="#">Contrato</a>
                    </li>
                  </ul>
                </div>
                <div margin="left">
                    <ul>
                         <a class="nav-link" href="index.php">SAIR</a>
                    </ul>
                </div>
              </nav>
       
               <!----> 
            
                <!-- Address form -->
                <div class="container">  
                <form class="form-horizontal" method="POST" action="actions/CadLocatario.php">
                     <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6"><br><br>
                 <div align="center" class="container">
                    <h3> Seja Bem Vindo Locatario</h3>
                </div>
                <h2 align="center"> CONTRATO DE LOCAÇÃO DE IMÓVEL RESIDENCIAL</h2> <bR><br>
                
            </div> 
        </div>
                       
                            <!-- Locador-->
                <div class="row">
                    <div class="col-md-3 field-label-responsive">
                        <b><label for="name">Locatário:</label></b>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                 <div class="input-group-addon" style="width: 2.6rem"></div><br>
                                <input type="text" name="locatario" class="form-control" id="locatario"  required autofocus>
                            </div>
                        </div>
                    </div> 
                </div>

                            

                            <!--ESTADO CIVIL-->
                <div class="row">
                     <div class="col-md-3 field-label-responsive">
                        <b><label for="name">Estado Civil:</label></b>
                    </div>
                        <div class="col-md-6">
                         <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                    <select id="estCivil" name="estCivil" class="input-xlarge">
                                        <option value="" selected="selected">Selecione seu Estado Civil</option>
                                        <option value="solteiro">Solteiro</option>
                                        <option value="casado">Casado</option>
                                        <option value="divorciado">Divociado</option>
                                    </select><br>
                                       
                            </div>
                         </div>
                        </div>
                </div>
                            
            
                            <!-- address-line2 input-->
                            <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">Profissão:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="text" name="profissao" class="form-control" id="profissao" placeholder="" required autofocus>
                             </div>
                         </div>
                     </div>
                  </div>
                            <!-- RG-->
                            <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">RG:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="text" name="rg" class="form-control" id="rg" placeholder="0000000-0" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>
                            <!-- postal-code input-->
                           <div class="row">
                     <div class="col-md-3 field-label-responsive">
                         <b><label for="name">CPF:</label></b>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                <input type="text" name="codPostal" class="form-control" id="codPostal" placeholder="" required autofocus><bR>
                             </div>
                         </div>
                     </div>
                  </div>
                            <!-- country select -->
                            <div class="row">
                     <div class="col-md-3 field-label-responsive">
                               <b> <label for="name">Estado:</label></b>
                               </div>
                                       <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"></div>
                                    <select id="estado" name="estado" class="input-xlarge">
                                        <option value="" selected="selected">Selecione seu Estado</option>
                                        <option value="AF">Acre</option>
                                        <option value="AL">Alagoas</option>
                                        <option value="AL">Amapá</option>
                                        <option value="AL">Amazonas</option>
                                        <option value="AL">Bahia </option>
                                        <option value="AL">Ceará</option>
                                        <option value="AL">Distrito Federal</option>
                                        <option value="AL">Espírito Santo</option>
                                        <option value="AL">Goiás</option>
                                        <option value="AL">Maranhão</option>
                                        <option value="AL">Mato Grosso</option>
                                        <option value="AL">Mato Grosso do Sul</option>
                                        <option value="AL">Minas Gerais</option>
                                        <option value="AL">Pará</option>
                                        <option value="AL">Paraíba </option>
                                        <option value="AL">Paraná</option>
                                        <option value="AL">Pernambuco </option>
                                        <option value="AL">Piauí</option>
                                        <option value="AL">Rio de Janeiro</option>
                                        <option value="AL">Rio Grande do Norte</option>
                                        <option value="AL">Rio Grande do Sul</option>
                                        <option value="AL">Rondônia</option>
                                        <option value="AL">Roraima</option>
                                        <option value="AL">Santa Catarina</option>
                                        <option value="AL">São Paulo</option>
                                        <option value="AL">Sergipe</option>
                                        <option value="AL">Tocantins </option>
                                    </select><br>     
                            </div>
                        </div>
                     </div>
                            </div>

                             <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
            <br><button  type="submit" class="btn btn-success" style="margin-left:40px"><i class="fa "></i>Enviar</button><br><br>
            </div>
        
        </div>
 
                                
                                <script language="javascrip">
    
    function imprime(text){
    text=document
    print(text) 
}

</script>

 

            </fieldset>
                     </form>
            </div>
            </div>
            
            </body>
            
            </html>