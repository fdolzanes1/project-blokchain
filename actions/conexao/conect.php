<?php
class conexaobd{
    static function conectar(){
        $host = "localhost";
        $db = "tcc";
        $usuario = "root";
        $senha = "";

        try{
            $conn = new PDO("mysql:host=$host;dbname=$db",$usuario,$senha);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "";
            return $conn;
        }catch(Exception $e){
            echo "Erro de conexão".$e->getMessage();
        }
    }
}

?>